ruby-bsearch (1.5-12) UNRELEASED; urgency=medium

  * Trim trailing whitespace.
  * Update standards version to 4.6.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 19 Nov 2022 17:13:53 -0000

ruby-bsearch (1.5-11) unstable; urgency=medium

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files
  * Set fully debhelper compatibility level to 9

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Cédric Boutillier ]
  * Update team name
  * Add .gitattributes to keep unwanted files out of the source package

  [ TANIGUCHI Takaki ]
  * d/source: Switch to format 3.0 (quilt) (Closes: #1007583)
  * Bump debian-compat to 13
  * d/control: Remove obsolete ruby-intepreter
  * d/control: Bump Standards-Version to 4.6.0
  * d/control: Add Vcs-* fields.
  * d/rules: convert encoding to utf-8

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 08 Apr 2022 11:36:42 +0900

ruby-bsearch (1.5-10) unstable; urgency=medium

  * Team upload.
  * Remove obsolete transitional packages
  * d/control: move homepage to Homepage field
  * Bump Standards-Version (no further changes)

 -- Christian Hofstaedtler <zeha@debian.org>  Wed, 08 Apr 2015 16:54:17 +0200

ruby-bsearch (1.5-9) unstable; urgency=low

  * debian/control:
    - Fixed typo in the depend on ruby-interpreter
    - Prefer ruby over a generic Ruby interpreter.
    - Added myself to the uploaders.
    - Bumped standards version to 3.9.3; no changes required.
    - Ensure that the section of the transitional packages is "extra".

 -- Paul van Tilburg <paulvt@debian.org>  Tue, 29 May 2012 20:40:26 +0200

ruby-bsearch (1.5-8) unstable; urgency=low

  * debian/install: Fix dir path.

 -- TANIGUCHI Takaki <takaki@debian.org>  Tue, 17 May 2011 10:57:09 +0900

ruby-bsearch (1.5-7) unstable; urgency=low

  * debian/control: change Maintainer to Debian Ruby Extra Maintainers.
  * Bump Standards-Version 3.9.2
  * Build with New Ruby policy.
      + debian/rules: switch to dh7 style.
      + Remove obsolete files debian/{dh_ruby, install.rb}.

 -- TANIGUCHI Takaki <takaki@debian.org>  Tue, 17 May 2011 10:34:54 +0900

ruby-bsearch (1.5-6) unstable; urgency=low

  * New maintainer (Closes: #541080)
  * update Policy-3.8.3

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 20 Nov 2009 17:52:48 +0900

ruby-bsearch (1.5-5) unstable; urgency=low

  * fix DESTDIR

 -- Fumitoshi UKAI <ukai@debian.or.jp>  Sun,  5 Oct 2003 21:56:18 +0900

ruby-bsearch (1.5-4) unstable; urgency=low

  * copy Ruby's license from ruby source.

 -- Fumitoshi UKAI <ukai@debian.or.jp>  Tue, 16 Sep 2003 12:58:17 +0900

ruby-bsearch (1.5-3) unstable; urgency=low

  * rename to libbsearch-ruby1.8
  * depends on libruby1.8

 -- Fumitoshi UKAI <ukai@debian.or.jp>  Mon,  8 Sep 2003 00:30:40 +0900

ruby-bsearch (1.5-2) unstable; urgency=low

  * add ruby in build-depends:
    closes: Bug#142143

 -- Fumitoshi UKAI <ukai@debian.or.jp>  Wed, 10 Apr 2002 23:40:08 +0900

ruby-bsearch (1.5-1) unstable; urgency=low

  * New upstream release

 -- Fumitoshi UKAI <ukai@debian.or.jp>  Mon, 10 Dec 2001 03:13:58 +0900

ruby-bsearch (1.4-3) unstable; urgency=low

  * bsearch.rb: add iterators

 -- Fumitoshi UKAI <ukai@debian.or.jp>  Mon, 10 Dec 2001 02:25:35 +0900

ruby-bsearch (1.4-2) unstable; urgency=low

  * debian/control (Description): fix typo

 -- Fumitoshi UKAI <ukai@debian.or.jp>  Tue, 20 Nov 2001 19:06:54 +0900

ruby-bsearch (1.4-1) unstable; urgency=low

  * new upstream

 -- Fumitoshi UKAI <ukai@debian.or.jp>  Fri,  9 Nov 2001 16:01:18 +0900

ruby-bsearch (1.2-1) unstable; urgency=low

  * Initial Release. closes: Bug#105522

 -- Fumitoshi UKAI <ukai@debian.or.jp>  Tue, 17 Jul 2001 01:56:32 +0900
